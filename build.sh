#!/bin/bash
# Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

set -e

python3 -m venv venv
# shellcheck source=/dev/null
source ./venv/bin/activate

# wheel needs to be installed before trying to install other dependencies
python3 -m pip install wheel
python3 -m pip install west pyelftools

mkdir -p __build

export BSIM_OUT_PATH=${PWD}/__build
export BSIM_COMPONENTS_PATH=${PWD}/babblesim/components

echo "Fetching babblesim"

if [ ! -d "babblesim" ];
then
    mkdir -p babblesim
    cd babblesim
    curl https://storage.googleapis.com/git-repo-downloads/repo > ./repo
    chmod a+x ./repo
    ./repo init -u https://github.com/BabbleSim/manifest.git -m everything.xml -b master
    # only pull the components we need
    ./repo sync -m "$(pwd)"/../BLE_demo_babblesim_requirements.xml
    cd ..
else
    echo "Skipping download, babblesim exists"
fi

pushd babblesim

echo "Building babblesim"

make everything -j 8

cp "${BSIM_COMPONENTS_PATH}"/common/stop_bsim.sh "${BSIM_OUT_PATH}"/bin

popd # root dir

echo "Building controller"

pushd zephyr-controller

if [ ! -d "__build/modules" ];
then
    mkdir -p __build
    cd __build
    west init --mr zephyr-v3.2.0
    cp ../west.yml zephyr/
    west update
    west zephyr-export
    pushd modules/hal/nordic/
    # we need to set email for git credential check caused by `git am`
    EMAIL=root@localhost git am ../../../../0001-Rename-a-variable-whose-name-conflict.patch
    popd
else
    echo "Skipping zephyr update through west"
    cd __build
fi

# because of hardcoded relative paths we need to symlink this in the west build directory
if [ ! -L "boards" ]; then ln -s ../boards boards; fi

# because of hardcoded relative paths we need to symlink this in the west build directory
if [ ! -L "hci_uart" ]; then ln -s ../hci_uart hci_uart; fi
cmake -B "$(pwd)"/hci_uart_build "$(pwd)"/hci_uart -DBOARD_ROOT="$(pwd)" -DBOARD=nrf52_bsim -DZEPHYR_TOOLCHAIN_VARIANT=host
cd hci_uart_build
make
cp zephyr/zephyr.elf "${BSIM_OUT_PATH}"/bin/bs_nrf52_bsim_samples_hci_uart
cd ..

for sample in "observer" "broadcaster";
do
    # because of hardcoded relative paths we need to symlink this in the west build directory
    if [ ! -L "${sample}" ]; then ln -s ./zephyr/samples/bluetooth/${sample} ${sample}; fi
    cmake -B "$(pwd)"/${sample}_build "$(pwd)"/${sample} -DBOARD_ROOT="$(pwd)" -DBOARD=nrf52_bsim -DZEPHYR_TOOLCHAIN_VARIANT=host
    cd ${sample}_build
    make
    cp zephyr/zephyr.elf "${BSIM_OUT_PATH}"/bin/bs_nrf52_bsim_samples_${sample}
    cd ..
done

popd # root dir

echo "Building HCI bridge"

# build linux bridge that connects loopback socket to hci uart interface
pushd __build

mkdir -p hci-ip-bridge
cd hci-ip-bridge
cmake ../../hci-ip-bridge
make
mv hci-ip-bridge "${BSIM_OUT_PATH}"/bin/

popd # root dir

deactivate
