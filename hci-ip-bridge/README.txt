This is meant to be the second half of the bridge enabling the bluetooth host (running in the FVP) to connect to a bluetooth controler (on the system hosting the FVP).

This listens on a loopback socket (port 51051) to which the FVP can connect.
It will then connect to the system HCI interface and relay the data.
