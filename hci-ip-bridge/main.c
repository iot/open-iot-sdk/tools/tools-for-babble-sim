/*
 * Copyright (c) 2022-2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

// tcp socket
#include <unistd.h>
#include <sys/socket.h>
#include <sys/errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>

// hci socket
#include <sys/ioctl.h>

#define BTPROTO_HCI      1
#define HCI_CHANNEL_RAW  0
#define HCI_CHANNEL_USER 1

#define HCIDEVUP      _IOW('H', 201, int)
#define HCIDEVDOWN    _IOW('H', 202, int)
#define HCIDEVRESET   _IOW('H', 203, int)
#define HCIGETDEVLIST _IOR('H', 210, int)

#define NIMBLE_DEFAULT_PORT 51051

struct sockaddr_hci {
    sa_family_t hci_family;
    unsigned short hci_dev;
    unsigned short hci_channel;
};

typedef struct {
    int socket_host;
    int socket_controller;
    int is_host_to_controller; // if false, direction is controller to host
} RelayParameter_t;

int init_bluetooth_socket(short hci_dev_number)
{
    struct sockaddr_hci shci;
    int s;
    int rc;

    memset(&shci, 0, sizeof(shci));
    shci.hci_family = AF_BLUETOOTH;
    shci.hci_dev = hci_dev_number;
    shci.hci_channel = HCI_CHANNEL_USER;

    s = socket(PF_BLUETOOTH, SOCK_RAW, BTPROTO_HCI);
    if (s < 0) {
        printf("socket() failed %d\n", errno);
        return -1;
    }

    /*
     * HCI User Channel requires exclusive access to the device.
     * The device has to be down at the time of binding.
     */
    ioctl(s, HCIDEVDOWN, shci.hci_dev);

    rc = bind(s, (struct sockaddr *)&shci, sizeof(shci));
    if (rc) {
        printf("bind() failed %d hci%d\n", errno, shci.hci_dev);
        if (s >= 0) {
            close(s);
        }
        return -1;
    }

    return s;
}

int init_server_socket(int port)
{
    struct sockaddr_in sin;
    memset(&sin, 0, sizeof(sin));
    sin.sin_family = AF_INET;
    sin.sin_port = htons(port);
    sin.sin_addr.s_addr = inet_addr("127.0.0.1");

    int sock = socket(PF_INET, SOCK_STREAM, 0);
    if (sock < 0) {
        printf("failed to create socket : %d\r\n", sock);
        goto error;
    }

    const int enable = 1;
    int status = setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int));
    if (status) {
        printf("failed to set SO_REUSEADDR on socket : %d\r\n", status);
        goto error_need_close;
    }

    status = bind(sock, (struct sockaddr *)&sin, sizeof(sin));
    if (status) {
        printf("failed to bind socket : %d\r\n", status);
        goto error_need_close;
    }

    status = listen(sock, 1u);
    if (status) {
        printf("failed to listen on socket : %d\r\n", status);
        goto error_need_close;
    }

    struct sockaddr remote_adress;
    socklen_t remote_adress_len;
    int remote_sock = accept(sock, &remote_adress, &remote_adress_len);
    if (remote_sock < 0) {
        printf("failed to accept socket : %d\r\n", remote_sock);
        goto error_need_close;
    }
    printf("connected !\n");

    close(sock);

    return remote_sock;

error_need_close:
    status = close(sock);
    if (status) {
        printf("failed to close listening socket : %d\r\n", status);
    }
error:
    printf("Error while processing the server socket\r\n");
    return -1;
}

void *relay_task(void *arg)
{
    int inbound_socket;
    int outbound_socket;
    RelayParameter_t *parameters = (RelayParameter_t *)(arg);
    if (parameters->is_host_to_controller) {
        inbound_socket = parameters->socket_host;
        outbound_socket = parameters->socket_controller;
    } else {
        inbound_socket = parameters->socket_controller;
        outbound_socket = parameters->socket_host;
    }

    char buff[2048];
    while (1) {
        int res = read(inbound_socket, buff, sizeof(buff) - 1);
        if (res < 0) {
            return NULL; // Receive error, likely the socket is closed
        }
        if (res > 0) {
            if (parameters->is_host_to_controller) {
                printf("host -> controller ");
            } else {
                printf("controller -> host ");
            }
            printf("received len %d :", res);
            for (int i = 0; i < res; i++) {
                printf(" %d", buff[i]);
            }
            printf("\n");

            int remaining = res;
            char *buff_ptr = buff;
            while (remaining > 0) {
                res = send(outbound_socket, buff_ptr, remaining, 0);
                if (res < 0) {
                    return NULL;
                }
                remaining -= res;
                buff_ptr += res;
            }
        }
    }
}

void usage(const char *prog_name)
{
    printf("Usage : %s [hci_dev_number]\n for hci0, use 0, for hci 1, use 1... Default to use hci1", prog_name);
}

int main(int argc, char *argv[])
{
    short hci_dev_number;
    if (argc == 1) {
        hci_dev_number = 1;
    } else if (argc == 2) {
        char *tmpptr;
        hci_dev_number = strtol(argv[1], &tmpptr, 10);
        if (*tmpptr != '\0') {
            usage(argv[0]);
            return EXIT_FAILURE;
        }
    } else {
        usage(argv[0]);
        return EXIT_FAILURE;
    }

    int bt_socket = init_bluetooth_socket(hci_dev_number);
    if (bt_socket == -1) {
        return EXIT_FAILURE;
    }
    printf("bt socket bound\n");

    int server_socket = init_server_socket(NIMBLE_DEFAULT_PORT);
    if (server_socket == -1) {
        return EXIT_FAILURE;
    }

    pthread_t relay_threads[2];

    RelayParameter_t host_controller_parameter = {server_socket, bt_socket, 1};
    pthread_create(&relay_threads[0], NULL, relay_task, &host_controller_parameter);

    RelayParameter_t controller_host_parameter = {server_socket, bt_socket, 0};
    pthread_create(&relay_threads[1], NULL, relay_task, &controller_host_parameter);

    while (1) {
        int status = pthread_tryjoin_np(relay_threads[0], NULL);
        if (status != EBUSY) {
            // host controller thread have returned
            printf("host -> controller relay closed\n");
            break;
        }
        status = pthread_tryjoin_np(relay_threads[1], NULL);
        if (status != EBUSY) {
            // controller host thread have returned
            printf("controller -> host relay closed\n");
            break;
        }
        sleep(1);
    }
    return EXIT_SUCCESS;
}
