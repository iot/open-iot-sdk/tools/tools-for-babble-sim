<!--
   NOTE: Do not remove any of the template headings.
-->
This issue should follow our [contributing guidelines](https://gitlab.arm.com/iot/open-iot-sdk/sdk/-/blob/main/CONTRIBUTING.md).
Additional guidelines can be found [here](https://gitlab.arm.com/iot/open-iot-sdk/sdk/-/tree/main/docs/guidelines).

### Description of defect

<!--
   Add detailed description of what you are reporting and how it can be reproduced.
-->

#### Specifications (toolchain, platform, component)
