<!--
    NOTE: Do not remove any of the template headings.
-->
This merge request should follow our [contributing guidelines](https://gitlab.arm.com/iot/open-iot-sdk/sdk/-/blob/main/CONTRIBUTING.md).
Additional guidelines can be found [here](https://gitlab.arm.com/iot/open-iot-sdk/sdk/-/tree/main/docs/guidelines).

- [ ] I confirm by raising this contribution that I have considered the security implications in [secure_coding.md](https://gitlab.arm.com/iot/open-iot-sdk/sdk/-/tree/main/docs/guidelines/secure_coding.md)

-------------------------------------------------------------------------------
### Description of proposed changes <!-- Required -->
