#!/bin/bash
# Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

# \x03 is an interrupt signal, we use it to stop the test
/usr/bin/expect -c '
  spawn ./run.sh selftest
  expect "Device found:"
  send \x03
'
