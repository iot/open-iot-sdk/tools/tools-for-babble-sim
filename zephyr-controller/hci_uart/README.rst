.. _bluetooth-hci-uart-sample:

Bluetooth: BLUEZ BABBLESIM
####################

Overview
*********

Expose the Zephyr Bluetooth controller support over UART to another device/CPU
using the H:4 HCI transport protocol (requires HW flow control from the UART).
This is a variant of the hci_uart that replace interrupts by polling, thus
enabling us to use the nrf52_bsim board

Requirements
************

None

Default UART settings
*********************

By default the controller builds use the following settings:

* Baudrate: 1Mbit/s
* 8 bits, no parity, 1 stop bit
* Hardware Flow Control (RTS/CTS) enabled

Building and Running
********************

This sample can be found under :zephyr_file:`samples/bluetooth/BlueZ_Babblesim` in the
Zephyr tree, and it is built as a standard Zephyr application.

Using the controller with emulators and BlueZ
*********************************************

The instructions below show how to use a Nordic nRF5x device as a Zephyr BLE
controller and expose it to Linux's BlueZ. This can be very useful for testing
the Zephyr Link Layer with the BlueZ Host. The Zephyr BLE controller can also
provide a modern BLE 5.0 controller to a Linux-based machine for native
BLE support or QEMU-based development.

First, make sure you have a recent BlueZ version installed by following the
instructions in the :ref:`bluetooth_bluez` section.

Now build the sample for the babblesim compatible board :

.. zephyr-app-commands::
   :zephyr-app: samples/bluetooth/BlueZ_Babblesim
   :board: nrf52_bsim
   :goals: build


.. _bluetooth-hci-uart-bluez:

Using the controller with BlueZ
===============================

In order to use the HCI UART controller with BlueZ you will need to attach it
to the Linux Host first. To do so simply build the sample and connect the
UART to the Linux machine, and then attach it with this command:

.. code-block:: console

   sudo btattach -B /dev/pts/2 -S 1000000

.. note::
   Depending on the serial port you are using you will need to modify the
   ``/dev/pts/2`` string to point to the serial device your controller is
   connected to.

If you are running :file:`btmon` you should see a comprehensive log showing how
BlueZ loads and initializes the attached controller.

Once the controller is attached follow the instructions in the
:ref:`bluetooth_ctlr_bluez` section to use BlueZ with it.
