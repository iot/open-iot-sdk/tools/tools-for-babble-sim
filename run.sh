#!/bin/bash
# Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

cleanup()
{
    echo "Stopping other processes"
    ./stop_bsim.sh
    cd .. # __build
    # remove files created by babblesim
    rm -rf results
    exit 0
}

# in selftest mode instead of HCI uart controllers we launch an observer and advertiser
# to check if babblesim works
if [ "$1" == "selftest" ] ; then
    BABBLESIM_CLIENT1=bs_nrf52_bsim_samples_broadcaster
    BABBLESIM_CLIENT2=bs_nrf52_bsim_samples_observer
else
    BABBLESIM_CLIENT1=bs_nrf52_bsim_samples_hci_uart
    BABBLESIM_CLIENT2=bs_nrf52_bsim_samples_hci_uart
fi

cd __build/bin || exit 1

./bs_2G4_phy_v1 -s=hci-bridge-test -D=3 &
./bs_device_handbrake -s=hci-bridge-test -d=0 -r=1 &
./$BABBLESIM_CLIENT1 -s=hci-bridge-test -d=1 &

trap cleanup SIGINT SIGTERM
# this will run until terminated
./$BABBLESIM_CLIENT2 -s=hci-bridge-test -d=2
