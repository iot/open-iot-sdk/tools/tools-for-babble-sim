
# Babblesim tools

## Dependencies

- Python 3 with virtual environment and pip
- curl
- git
- CMake (3.20 at minimum)
- GCC or Clang with multilib support

If you're running an debian derivative you may use the command line below:

```sh
apt-get -y install python3 python3-venv python3-pip curl git cmake gcc-multilib
```

Note that CMake 3.20 at minimum is required to build zephyr.
If you're running an older distro check (Kitware's website)[https://apt.kitware.com/] for details on how to add a
PPA with a newer cmake.

## Components

babblesim - simulation which connects the controllers together.
hci-ip-bridge - a relay listening on an IP socket to forward HCI traffic to an HCI interface.
zephyr-controller - used to build a controller part of the BLE stack that waits for you to connect your host stack to.

## How to build

Run the top level build script:

```sh
./build.sh
```

## How to run

### Running Babblesim


The run.sh script is setup to run with two UART HCI connected controllers.
If you want to run a different set of controllers you may run babblesim yourself instead of running the script.

Run the top level run script as sudo:

```sh
sudo ./run.sh
```

Babblesim is now connected to two controllers waiting for host stacks to connect to them.

Take note of the pseudotty used, eg: ```UART_0 connected to pseudotty: /dev/pts/5```

Leave it running and open a new terminal.

Use the pseudotty you got from babblesim to attach an HCI interface to a TTY.

```sh
sudo btattach -P h4 -B /dev/pts/5 -S 1000000
```

Take note of the device ID, eg: ```Device index 1 attached```

This will match the HCI interface ID you can later use to connect to using your host stack (`HCI1` in this case).

#### Monitoring HCI traffic

If you want to be sure the `btattach` is complete you may monitor the traffic in another terminal with:


```sh
sudo btmon
```

### Connecting Host stacks to Controllers

Controllers are now waiting for host stacks to connect to them over HCI.

#### HCI over IP socket

You can use hci-bridge to connect your host over a ip socket transport.

The first parameter parameter is the device ID you previously obtained from running btattach.

```sh
sudo __build/bin/hci-ip-bridge 1
```

The bridge is listening on a socket bound to loopback and port 51051.

Leave the terminal running and observe the traffic when you connect to it over the IP socket.

#### Other host stacks

Any host stack can connect to the created HCI interfaces.
You may for example use bleak to write python scripts which will connect to the controllers running in babblesim.

# License and contributions

The software is provided under the Apache-2.0 license. All contributions to software and documents are licensed by contributors under the same license model as the software/document itself (ie. inbound == outbound licensing). Open IoT SDK may reuse software already licensed under another license, provided the license is permissive in nature and compatible with Apache v2.0.

Folders containing files under different permissive license than Apache 2.0 are listed in the LICENSE file.

Please see [CONTRIBUTING.md](CONTRIBUTING.md) for more information.

## Security issues reporting

If you find any security vulnerabilities, please do not report it in the GitLab issue tracker. Instead, send an email to the security team at arm-security@arm.com stating that you may have found a security vulnerability in the Open IoT SDK.

More details can be found at [Arm Developer website](https://developer.arm.com/support/arm-security-updates/report-security-vulnerabilities).
