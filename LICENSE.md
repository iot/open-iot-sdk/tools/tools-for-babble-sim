Unless specifically indicated otherwise in a file, files are licensed under the Apache 2.0 license,
as can be found in: LICENSE-apache-2.0.txt

## Components

Folders containing external components are listed below. Each component should contain its own README file with license specified for its files. The original license text is included in those source files.

```json:table
{
    "fields": [
        "Component",
        "Path",
        "License",
        "Origin",
        "Category",
        "Version",
        "Security risk"
    ],
    "items": [
        {
            "Component": "Babblesim base",
            "Path": "babblesim/components/common",
            "License": "Apache-2.0",
            "Origin": "https://github.com/BabbleSim/base",
            "Category": "2",
            "Version": "02838ca04c4562e68dc876196828d8121679e537",
            "Security risk": "low"
        },
        {
            "Component": "Babblesim modem_magic",
            "Path": "babblesim/components/ext_2G4_modem_magic",
            "License": "Apache-2.0",
            "Origin": "https://github.com/BabbleSim/ext_2G4_modem_magic",
            "Category": "2",
            "Version": "cb70771794f0bf6f262aa474848611c68ae8f1ed",
            "Security risk": "low"
        },
        {
            "Component": "Babblesim channel_NtNcable",
            "Path": "babblesim/components/ext_2G4_channel_NtNcable",
            "License": "Apache-2.0",
            "Origin": "https://github.com/BabbleSim/ext_2G4_channel_NtNcable",
            "Category": "2",
            "Version": "20a38c997f507b0aa53817aab3d73a462fff7af1",
            "Security risk": "low"
        },
        {
            "Component": "Babblesim phy_v1",
            "Path": "babblesim/components/ext_2G4_phy_v1",
            "License": "Apache-2.0",
            "Origin": "https://github.com/BabbleSim/ext_2G4_phy_v1",
            "Category": "2",
            "Version": "cf2d86e736efac4f12fad5093ed2da2c5b406156",
            "Security risk": "low"
        },
        {
            "Component": "Babblesim libPhyComv1",
            "Path": "babblesim/components/ext_2G4_libPhyComv1",
            "License": "Apache-2.0",
            "Origin": "https://github.com/BabbleSim/ext_2G4_libPhyComv1",
            "Category": "2",
            "Version": "9018113a362fa6c9e8f4b9cab9e5a8f12cc46b94",
            "Security risk": "low"
        },
        {
            "Component": "zephyr",
            "Path": "zephyr-controller/__build/zephyr",
            "License": "Apache-2.0",
            "Origin": "https://github.com/zephyrproject-rtos/zephyr",
            "Category": "2",
            "Version": "v3.2.0",
            "Security risk": "low"
        },
        {
            "Component": "zephyr hci_uart & board",
            "Path": "zephyr-controller/",
            "License": "Apache-2.0",
            "Origin": "https://github.com/zephyrproject-rtos/zephyr",
            "Category": "1",
            "Version": "v3.2.0",
            "Security risk": "low"
        }
    ]
}
```
